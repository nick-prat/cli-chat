#include "server.hh"
#include "socket.hh"

#include <cctype>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>
#include <thread>
#include <bit>

#include "client.hh"
#include "strutil.hh"

void read_func(std::stop_token stoken, int fd) {
    auto buff = new uint8_t[MSG_PACKET_SIZE];
    
    while ( ! stoken.stop_requested() ) {
        auto res = recv(fd, buff, MSG_PACKET_SIZE, 0);

        // Disconnection on error other than a retry
        if ( res == 0 || ( res == -1 && errno != EAGAIN ) ) {
            break;
        } else if ( res == -1 ) {
            continue;
        }

        auto packet = decode_packet(buff);

        std::cout << packet.buff << '\n';

        memset(buff, 0, MSG_PACKET_SIZE);
    }

    delete [] buff;
}

client_handler::client_handler()
: m_fd{socket(AF_INET, SOCK_STREAM, 0)} {
    if ( m_fd == -1 )
        throw std::runtime_error{"failed to create socket"};
}

client_handler::~client_handler() {
    if ( m_fd != -1)
        shutdown();
}

int client_handler::connect(std::string const& ip, std::uint16_t const port) {
    auto saddr = generate_sockaddr(ip.c_str(), port);
    if ( ::connect(m_fd, reinterpret_cast<sockaddr*>(&saddr), sizeof(decltype(saddr))) != 0 )
        return errno;

    m_connected = true;
    m_read_thread = std::jthread(&read_func, m_fd);
    return 0;
}

void client_handler::send_message(std::string const& msg) const {
    send(msg, msg_type::message);
}

void client_handler::shutdown() {
    ::shutdown(m_fd, SHUT_RDWR);
    m_read_thread.request_stop();
    m_read_thread.join();
    close(m_fd);
    m_fd = -1;
}

void client_handler::send(std::string const& msg, msg_type type) const {
    if ( ! m_connected )
        return;
    
    // TODO Actually handle this problem
    if ( msg.size() > MAX_MESSAGE_SIZE ) {
        std::cout << "Message was too big, skipping\n";
        return;
    }

    auto packet = encode_packet(msg, type);
    write(m_fd, &packet, MSG_PACKET_SIZE);
}

int main(int argc, char** argv) {
    client_handler ch{};

    auto exit = false;
    std::string line{};
    while ( exit != true ) {
        std::getline(std::cin, line);
        if ( line.empty() )
            continue;

        if ( line.at(0) == '/' ) {
            auto cmds = split_string(line.substr(1, line.size()), ' ');

            if ( cmds[0] == "exit" ) {
                exit = true;
            } else if ( cmds[0] == "connect" ) {
                if ( ch.connect(cmds[1], 9004) != 0 ) {
                    std::cout << "connection failure\n";
                }
            } else if ( cmds[0] == "login" ) {
                ch.send(cmds[1], msg_type::login);
            } else if ( cmds[0] == "whoami" ) {
                ch.send("", msg_type::whoami);
            } else if ( cmds[0] == "disconnect" ) {
                ch.shutdown();
            }

            continue;
        }

        ch.send_message(line);
    }

    ch.shutdown();
    return 0;
}