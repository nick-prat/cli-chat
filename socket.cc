#include "socket.hh"
#include <arpa/inet.h>
#include <cstring>

#include <cstdint>
#include <iostream>

msg_packet encode_packet(std::string const& msg, msg_type type) {
    msg_packet packet{};

    packet.header.size = htonl(msg.size());
    packet.header.type = type;
    memcpy(packet.buff, msg.data(), msg.size());

    return packet;
}

msg_packet decode_packet(std::uint8_t const* buff) {
    msg_packet packet{};

    memcpy(&packet, buff, sizeof(msg_packet));
    packet.header.size = ntohl(packet.header.size);

    return packet;
}

sockaddr_in generate_sockaddr(std::string const& ip, std::uint16_t const port) {
    sockaddr_in sockaddr{};
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    if ( inet_pton(AF_INET, ip.data(), &sockaddr.sin_addr.s_addr) != 1 ) {
        std::cout << "generate_sockaddr failure\n";
        exit(1);
    }
    return sockaddr;
}

sockaddr_in generate_sockaddr(std::uint32_t const ip, std::uint16_t const port) {
    sockaddr_in sockaddr{};
    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(port);
    sockaddr.sin_addr.s_addr = htonl(ip);
    return sockaddr;
}