CXX=clang++
CXXFLAGS=-std=c++20 -g
LDFLAGS=-luuid -pthread
DEPS=socket.hh

all: client server

client: client.o socket.o
	$(CXX) client.o socket.o -o client $(LDFLAGS)

server: server.o socket.o
	$(CXX) server.o socket.o -o server $(LDFLAGS)

clean:
	rm server client
	rm *.o