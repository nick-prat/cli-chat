
#include <algorithm>
#include <asm-generic/errno-base.h>
#include <asm-generic/errno.h>
#include <asm-generic/socket.h>
#include <atomic>
#include <chrono>
#include <cstdio>
#include <mutex>
#include <netinet/in.h>
#include <stdexcept>
#include <stop_token>
#include <sys/socket.h>
#include <unistd.h>

#include <thread>
#include <iostream>
#include <string>
#include <uuid/uuid.h>
#include <vector>
#include <cstring>
#include <sstream>

#include "server.hh"

#include "socket.hh"

using namespace std::literals;

class server_handler;

uuid::uuid() {
    uuid_generate_random(m_uuid);
}

uuid::uuid(uuid const& other) {
    memcpy(m_uuid, other.m_uuid, sizeof(uuid_t));
}

bool uuid::operator==(uuid const& other) const {
    return uuid_compare(m_uuid, other.m_uuid) == 0;
}

client_connection::client_connection(server_handler& server, int fd)
: m_server{server}, m_paused{false}, m_fd{fd} {
    m_thread = std::jthread([this](std::stop_token stoken) {
        handle(stoken);
    });
}

client_connection::~client_connection() {
    if ( m_fd != -1 )
        close(m_fd);
}

void client_connection::send_message(std::string const& message) const {
    auto packet = encode_packet(message, msg_type::message);
    write(m_fd, &packet, MSG_PACKET_SIZE);
}

void client_connection::join() {
    if ( m_thread.joinable() )
        m_thread.request_stop();
    m_thread.join();
}

int client_connection::get_fd() const {
    return m_fd;
}

uuid const& client_connection::get_uuid() const {
    return m_uuid;
}

std::string const& client_connection::get_username() const {
    return m_username;
}

void client_connection::pause() {
    m_paused = true;
}

void client_connection::resume() {
    m_paused = false;
}

void client_connection::handle(std::stop_token stoken) {
    auto buff = new std::uint8_t[MSG_PACKET_SIZE];
    
    while ( ! stoken.stop_requested() ) {
        auto res = recv(m_fd, buff, MSG_PACKET_SIZE, 0);

        // Disconnection on error other than a retry
        if ( res == 0 || ( res == -1 && errno != EAGAIN ) ) {
            break;
        } else if ( res == -1 ) {
            continue;
        }

        auto packet = decode_packet(buff);
        std::string msg{packet.buff, packet.buff + packet.header.size};
        std::cout << "[DEBUG]: Received message '" << msg << "' from '" << m_username << "' in " << packet.header.size << " bytes\n";

        switch(packet.header.type) {
        case msg_type::message:
            m_server.send_message(msg, m_uuid);
            break;
        case msg_type::login:
            m_username = packet.buff;
            break;
        case msg_type::whoami:
            send_message(m_username);
            break;
        default:
            break;
        }
        
        memset(buff, 0, MSG_PACKET_SIZE);
    }

    delete [] buff;

    m_server.close_connection(m_uuid);
}

server_handler::~server_handler() {
    m_closer_thread.request_stop();
    m_closer_thread.join();
}

void server_handler::run() {
    auto sock = socket(AF_INET, SOCK_STREAM, 0);
    if ( sock == -1 )
        throw std::runtime_error{"failed to create socket"};

    auto saddr = generate_sockaddr(INADDR_ANY, 9004);
    if ( bind(sock, reinterpret_cast<sockaddr*>(&saddr), sizeof(sockaddr_in)) != 0 )
        throw std::runtime_error{"failed to bind"};

    if ( listen(sock, 5) != 0 )
        throw std::runtime_error{"Listen failure"};

    sockaddr_in client_sockaddr{};
    socklen_t slen{};
    int fd{};

    // TODO Race condition if client connection is closed while a message is being sent
    m_closer_thread = std::jthread{[this](std::stop_token stoken) {
        while ( ! stoken.stop_requested() ) {
            if ( m_close_queue.size() > 0 ) {
                std::lock_guard<std::mutex> lg{m_close_mutex};
                auto id = m_close_queue.front();
                auto iter = std::find_if(m_client_connections.begin(), m_client_connections.end(), [id](auto& cc) {
                    return cc.get_uuid() == id;
                });
                iter->join();
                m_client_connections.erase(iter);
                m_close_queue.pop();
            }
            std::this_thread::sleep_for(std::chrono::milliseconds{250});
        }
    }};

    while ( (fd = accept(sock, reinterpret_cast<sockaddr*>(&client_sockaddr), &slen)) != -1 ) {
        std::cout << "Accepted client connection\n";
        create_connection(fd);
    }

    std::cout << "Closed " << strerror(errno) << '\n';
}

void server_handler::create_connection(int fd) {
    timeval tv{5, 0};
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<void*>(&tv), sizeof tv);

    m_client_connections.emplace_back(*this, fd);
}

client_connection& server_handler::get_connection(uuid const& id) {
    auto iter = std::find_if(m_client_connections.begin(), m_client_connections.end(), [&id](client_connection& cc) {
        return cc.get_uuid() == id;
    });

    if ( iter != m_client_connections.end() )
        return *iter;
    throw std::runtime_error{"client connection not found"};
}

client_connection const& server_handler::get_connection(uuid const& id) const {
    auto iter = std::find_if(m_client_connections.cbegin(), m_client_connections.cend(), [&id](client_connection const& cc) {
        return cc.get_uuid() == id;
    });

    if ( iter != m_client_connections.end() )
        return *iter;
    throw std::runtime_error{"client connection not found"};
}

void server_handler::close_connection(uuid const& id) {
    std::lock_guard<std::mutex> lg{m_close_mutex};
    get_connection(id).pause();
    m_close_queue.push(id);
}

void server_handler::send_message(std::string const& message, uuid const& originator) const {
    auto const& username = get_connection(originator).get_username();
    for ( auto const& client : m_client_connections ) {
        if ( client.get_uuid() == originator )
            continue;

        std::stringstream ss{};
        ss << '[' << username << "] " << message;
        client.send_message(ss.str());
    } 
}

int main(int argc, char** argv) {
    server_handler server{};

    server.run();    

    return 0;
}