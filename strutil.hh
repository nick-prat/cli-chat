#pragma once

#include <string>
#include <vector>
#include <sstream>

template<typename T>
void ltrim(std::basic_string<T>& str) {
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](auto c) {
        return !std::isspace(c);
    }));
}

template<typename T>
void rtrim(std::basic_string<T>& str) {
    str.erase(std::find_if(str.rbegin(), str.rend(), [](auto c) {
        return !std::isspace(c);
    }).base(), str.end());
}

template<typename T>
void trim(std::basic_string<T>& str) {
    ltrim(str);
    rtrim(str);
}

template<typename T>
std::vector<std::basic_string<T>> split_string(std::basic_string<T> const& string, T delim) {
    std::vector<std::basic_string<T>> strings;

    std::istringstream input{string};
    std::basic_string<T> token{};

    while ( std::getline(input, token, delim) ) {
        strings.push_back(std::move(token));
    }

    return strings;
}