#pragma once

#include "socket.hh"
#include <string>
#include <thread>

class client_handler {
public:
    client_handler();
    ~client_handler();

    void handle_command(std::string const& cmd);
    int connect(std::string const& ip, std::uint16_t const port);
    void send_message(std::string const& msg) const;
    void send(std::string const& msg, msg_type type) const;
    void shutdown();

private:
    bool m_connected;
    int m_fd;
    std::jthread m_read_thread;
};