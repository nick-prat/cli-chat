#pragma once

#include <uuid/uuid.h>

#include <atomic>
#include <stop_token>
#include <string>
#include <thread>
#include <list>
#include <queue>
#include <mutex>

class server_handler;

class uuid {
public:
    uuid();
    uuid(uuid const& other);
    bool operator==(uuid const& other) const;

private:
    uuid_t m_uuid;
};

class client_connection {
public:
    client_connection(server_handler& server, int fd);
    ~client_connection();
    client_connection(client_connection&&) = default;

    void send_message(std::string const& message) const;
    void join();
    int get_fd() const;
    uuid const& get_uuid() const;
    std::string const& get_username() const;

    void pause();
    void resume();

private:
    void handle(std::stop_token stoken);

private:
    server_handler& m_server;
    std::string m_username;
    bool m_paused;
    int m_fd;
    uuid m_uuid;
    std::jthread m_thread;
};

class server_handler {
public:
    server_handler() = default;
    ~server_handler();

    void run();

    void create_connection(int fd);
    client_connection& get_connection(uuid const& fd);
    client_connection const& get_connection(uuid const& fd) const;
    void close_connection(uuid const& id);
    void send_message(std::string const& message, uuid const& originator) const;

private:
    std::list<client_connection> m_client_connections;
    std::jthread m_closer_thread;
    std::queue<uuid> m_close_queue;
    std::mutex m_close_mutex;
};