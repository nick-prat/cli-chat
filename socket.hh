#pragma once

#include <cstdint>
#include <netinet/in.h>

#include <string>

enum class msg_type : std::uint8_t {
    message,
    command,
    login,
    whoami
};

struct msg_header {
    std::uint32_t size;
    msg_type type;
};

struct msg_packet {
    msg_header header;
    char buff[1024];
};

constexpr auto MSG_PACKET_SIZE = sizeof(msg_packet);
constexpr auto MAX_MESSAGE_SIZE = sizeof(msg_packet::buff);

msg_packet encode_packet(std::string const& msg, msg_type type);
msg_packet decode_packet(std::uint8_t const* buff);

sockaddr_in generate_sockaddr(std::string const& ip, std::uint16_t const port);
sockaddr_in generate_sockaddr(std::uint32_t const ip, std::uint16_t const port);